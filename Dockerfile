FROM node

EXPOSE 3000

RUN mkdir /app
WORKDIR /app

COPY package.json /app
RUN yarn install

COPY . /app
RUN yarn test && yarn build

CMD yarn start
